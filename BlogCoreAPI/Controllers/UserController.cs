﻿using BlogCoreAPI.Dependence.Services;
using BlogCoreAPI.Model;
using BlogCoreAPI.Model.ViewModel;
using BlogCoreAPI.Specifications;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BlogCoreAPI.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        public IHostingEnvironment hostingEnvironment;
        private readonly ILogger<UserController> _logger;
        private IProductService _ProductService;
        public UserController(ILogger<UserController> logger,IHostingEnvironment hosting, IProductService productService)
        {
            hostingEnvironment = hosting;
            _logger = logger;
            _ProductService = productService;

        }
        //[Authorize(Roles = "admin")]
        [HttpGet("GetAllProduct")]
        public IEnumerable<Product> GetAllProduct(string search)
        {

            if (search != null)
            {
                var complete = new SearchProductSpecification(search);
                var products = _ProductService.GetAllProductBySearch(search);
                foreach (var product in products)
                {
                    if (product.IsDelete == false)
                    {
                        yield return product;
                    }
                }
            }
            else
            {
                var products = _ProductService.GetAllProduct();
                foreach (var product in products)
                {
                    if (product.IsDelete == false)
                    {
                        yield return product;
                    }
                }
            }
        }
        [HttpGet("GetProductById/{id}")]
        public IActionResult GetProductById(int id)
        {
            var product = _ProductService.GetProductById(id);
            product.View += 1;
            _ProductService.UpdateProduct(product);
            return Ok(product);
        }

        [HttpPost("Add")]
        public IActionResult Add(ProductView request, CancellationToken cancellationToken = default)
        {
            var item = new Product();
            item.Name = request.Name;
            item.StyleId = request.StyleId;
            item.PriceSales = request.PriceSales;
            item.Price = request.Price;
            item.IsDelete = request.IsDelete;
            item.CreateAt = DateTime.Now;
            item.Image = request.Image.ToString();
            var files = HttpContext.Request.Form.Files;
            if (files != null && files.Count > 0)
            {
                foreach (var file in files)
                {
                    FileInfo fi = new FileInfo(file.Name);
                    var newfilename = "Image_" + DateTime.Now.TimeOfDay.Milliseconds + fi.Extension;
                    var path = Path.Combine("", hostingEnvironment.ContentRootPath + "/Images/" + newfilename);
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                    _ProductService.AddNewProduct(item);
                }
              
            }
            return Ok();
        }
        [HttpPost("AddProduct")]
        public IActionResult AddNewProduct([FromForm]Product product)
        {
            var files = HttpContext.Request.Form.Files;
            if (files != null && files.Count > 0)
            {
                foreach (var file in files)
                {
                    FileInfo fi = new FileInfo(file.Name);
                    var newfilename = "Image_" + DateTime.Now.TimeOfDay.Milliseconds + fi.Extension;
                    var path = Path.Combine("", hostingEnvironment.ContentRootPath + "/Images/" + newfilename);
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                    _ProductService.AddNewProduct(product);
                }
            }
            
            return Ok();
        }

        [HttpPut("EditProduct/{id}")]
        public IActionResult EditProdyct([FromForm] Product product)
        {
            var existingItem = _ProductService.GetProductById(product.ProductId);
            if (existingItem == null) return BadRequest();
            else
            {
                _ProductService.UpdateProduct(product);
            }
            return Ok();
        }
        [HttpPut("DeleteProduct/{id}")]
        public IActionResult DeleteProduct(int id)
        {
            var existingItem = _ProductService.GetProductById(id);
            if (existingItem == null) return BadRequest();
            else
            {
                existingItem.IsDelete = true;
            }
            _ProductService.ChangeIsDelete(existingItem);
            return Ok();
        }
        
    
        //public IEnumerable<Product> GetProdcutById(int id)
        //{
        //    var check = new ProductByIDIncompleteIsDeleteSpecificationv(id);
        //    return _ProductService.GetAllProduct();
        //}
        /*  [HttpGet("/GetAllProduct")]
          public  async Task<ActionResult<Product>> HandleAsync(CancellationToken cancellationToken)
          {
              var createdItem =  _ProductService.GetAllProduct();
              return Ok(createdItem);
          }

          [HttpGet("/GetProductById")]
          public async Task<ActionResult<Product>> GetProDuctById(CancellationToken cancellationToken)
          {

          }
  */
    }
}
