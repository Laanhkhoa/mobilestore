﻿
using BlogCoreAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogCoreAPI.Dependence.Services.Interface
{
    public interface IUserService 
    {
        User GetUserById(int id);
        void AddNewUser(User user);
        void UpdateUser(User user);

    }
}
