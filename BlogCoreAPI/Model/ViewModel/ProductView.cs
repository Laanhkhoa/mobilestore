﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogCoreAPI.Model.ViewModel
{
    public class ProductView
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public int StyleId { get; set; }
        public int Quantity { get; set; } = 0;
        public IFormFile Image { get; set; }
        public double Price { get; set; }
        public double? PriceSales { get; set; }
        public ulong View { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime? UpdateAt { get; set; }
    }
}
